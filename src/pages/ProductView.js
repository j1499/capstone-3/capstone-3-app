// This is working
import { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
// import img from '../images';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

    const { user } = useContext(UserContext);


    const navigate = useNavigate();

    const { productId } = useParams();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(0);
    const [img, setImg] = useState('img');
    const [isActive, setIsActive] = useState('');
    const [image, setImage] = useState('')

    // Add to cart
    const [cart, setCart] = useState([]);

    // const addToCart = (product) => {

    // }


    // Function for purchase
    const purchase = (productId) => {

        fetch('https://limitless-waters-59300.herokuapp.com/users/checkout', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                name: name,
                description: description,
                price: price,
                quantity: quantity,
                image: image
            })
        })
            .then(res => res.json())
            .then(data => {

                // console.log(data, "This is the data from productView")

                if (data) {
                    Swal.fire({
                        title: "Successfully purchased",
                        icon: "success",
                        text: "You have successfully purchased this product."
                    })
                    navigate("/myorders")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })

    }

    // // Function for add to cart
    const addToCart = (productId) => {

        fetch('https://limitless-waters-59300.herokuapp.com/users/addToCart', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                name: name,
                description: description,
                price: price,
                quantity: quantity
            })
        })
            .then(res => res.json())
            .then(data => {

                // console.log(data, "This is the data from productView")

                if (data) {
                    Swal.fire({
                        title: "Successfully Added to Cart",
                        icon: "success",
                        text: "You have successfully added this product to cart."
                    })
                    navigate("/products")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })

    }

    // Testing function for add to cart
    // const addToCart = (product) => {
    //     // const [cartItems, setCartItems] = useState([])
    //     // Function for handling add to cart
    //     // const handleClick = (product) => {
    //     //     // console.log(product)
    //     //     // cartItems.push(product._id);
    //     //     // console.log(cartItems)
    //     //     console.log('Add to Cart')
    //     //     setCartItems([...cartItems, product])

    //     // }
    //     cart.push(product);
    //     setCart([...cart, product])
    // }

    // Function for quantity
    const incQty = () => {
        setQuantity(quantity + 1)
    }

    const decQty = () => {
        setQuantity(quantity - 1)
    }

    // Update product
    const update = (productId) => {
        fetch(`https://limitless-waters-59300.herokuapp.com/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                img: img,
                price: price,
                isActive: isActive
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data, "from update fetch")
                if (data) {
                    Swal.fire({
                        title: "Successfully edited",
                        icon: "success",
                        text: "You have successfully edited the item."
                    })


                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

                setName(data.name);
                setDescription(data.description);
                setImg(data.img);
                setPrice(data.price);
                setIsActive(data.isActive);
            })
    }



    // Non admin
    useEffect(() => {
        fetch(`https://limitless-waters-59300.herokuapp.com/products/${productId}`)
            .then(res => res.json())
            .then(data => {

                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setQuantity(data.quantity);
                setImg(data.img);
                setIsActive(data.isActive)

                decQty();
                incQty();
                // console.log(data, "from productView")
            })

    }, [productId])



    return (
        (user.isAdmin === false || user.id === null) ?
            <Container className="mt-5">
                <Row>
                    <Col lg={{ span: 6, offset: 3 }}>
                        <Card>
                            <Card.Body className="text-center">
                                <img src={img} className="card-img" />
                                <Card.Title>{name}</Card.Title>
                                <Card.Subtitle>Description:</Card.Subtitle>
                                <Card.Text>{description}</Card.Text>
                                <Card.Subtitle>Price:</Card.Subtitle>
                                <Card.Text>Php {price}</Card.Text>
                                <Card.Subtitle>Quantity</Card.Subtitle>
                                <Card.Text>
                                    <Button
                                        variant="outline-dark"
                                        className="quantity-btn"
                                        onClick={() => decQty()}
                                    >-</Button>
                                    {quantity}
                                    <Button
                                        variant="outline-dark"
                                        className="quantity-btn"
                                        onClick={() => incQty()}
                                    >+</Button>
                                </Card.Text>
                                {
                                    (user.id !== null) ?
                                        <div>
                                            <Button variant="primary" onClick={() => purchase(productId)}>Purchase</Button>
                                            <Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>
                                        </div>
                                        :
                                        <Link className="btn btn-danger" to="/login">Login to Purchase</Link>

                                }
                                {/* <Button variant="primary" onClick={() => purchase(productId)}>Purchase</Button> */}
                                {/* <Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button> */}
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
            :
            <div>
                <Container>
                    <h2>Update product info</h2>
                    <Form className="mt-3" onSubmit={() => update(productId)} >
                        <Form.Group className="mb-3" controlId="name">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter product name"
                                value={name}
                                onChange={e => setName(e.target.value)}
                                required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Product Description</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter product description"
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                                required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="userEmail">
                            <Form.Label>Image</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter image"
                                value={img}
                                onChange={e => setImg(e.target.value)}
                                required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Enter price</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter product price"
                                value={price}
                                onChange={e => setPrice(e.target.value)}
                                required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="isActive">
                            <Form.Label>IsActive?</Form.Label>
                            <Form.Control
                                type="text"
                                value={isActive}
                                onChange={e => setIsActive(e.target.value)}
                                required />
                        </Form.Group>


                        <Button variant="primary" type="submit" id="submitBtn">
                            Update
                        </Button>
                    </Form>

                </Container>
            </div >

    )

}
// This is working

// This is testing
// import { Fragment, useState, useEffect, useContext } from 'react';
// import { Container, Card, Row, Col, Button, Form } from 'react-bootstrap';
// import { useParams, useNavigate, Link } from 'react-router-dom';
// // import img from '../images';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

// export default function ProductView() {

//     const { user } = useContext(UserContext);


//     const navigate = useNavigate();

//     const { productId } = useParams();
//     const [name, setName] = useState('');
//     const [description, setDescription] = useState('');
//     const [price, setPrice] = useState(0);
//     const [quantity, setQuantity] = useState(0);
//     const [img, setImg] = useState('img');
//     const [isActive, setIsActive] = useState('');
//     const [image, setImage] = useState('')

//     // Add to cart
//     const [cart, setCart] = useState([]);

//     // const addToCart = (product) => {

//     // }


//     // Function for purchase
//     const purchase = (productId) => {

//         fetch('http://localhost:4000/users/checkout', {
//             method: 'POST',
//             headers: {
//                 "Content-Type": "application/json",
//                 Authorization: `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({
//                 productId: productId,
//                 name: name,
//                 description: description,
//                 price: price,
//                 quantity: quantity,
//                 image: image
//             })
//         })
//             .then(res => res.json())
//             .then(data => {

//                 // console.log(data, "This is the data from productView")

//                 if (data) {
//                     Swal.fire({
//                         title: "Successfully purchased",
//                         icon: "success",
//                         text: "You have successfully purchased this product."
//                     })
//                     navigate("/myorders")

//                 } else {
//                     Swal.fire({
//                         title: "Something went wrong",
//                         icon: "error",
//                         text: "Please try again."
//                     })
//                 }

//             })

//     }

//     // // Function for add to cart
//     const addToCart = (productId) => {

//         fetch('http://localhost:4000/users/addToCart', {
//             method: 'POST',
//             headers: {
//                 "Content-Type": "application/json",
//                 Authorization: `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({
//                 productId: productId,
//                 name: name,
//                 description: description,
//                 price: price,
//                 quantity: quantity
//             })
//         })
//             .then(res => res.json())
//             .then(data => {

//                 // console.log(data, "This is the data from productView")

//                 if (data) {
//                     Swal.fire({
//                         title: "Successfully Added to Cart",
//                         icon: "success",
//                         text: "You have successfully added this product to cart."
//                     })
//                     navigate("/products")

//                 } else {
//                     Swal.fire({
//                         title: "Something went wrong",
//                         icon: "error",
//                         text: "Please try again."
//                     })
//                 }

//             })

//     }

//     // Testing function for add to cart
//     // const addToCart = (product) => {
//     //     // const [cartItems, setCartItems] = useState([])
//     //     // Function for handling add to cart
//     //     // const handleClick = (product) => {
//     //     //     // console.log(product)
//     //     //     // cartItems.push(product._id);
//     //     //     // console.log(cartItems)
//     //     //     console.log('Add to Cart')
//     //     //     setCartItems([...cartItems, product])

//     //     // }
//     //     cart.push(product);
//     //     setCart([...cart, product])
//     // }

//     // Function for quantity
//     const incQty = () => {
//         setQuantity(quantity + 1)
//     }

//     const decQty = () => {
//         setQuantity(quantity - 1)
//     }

//     // Update product
//     const update = (productId) => {
//         fetch(`http://localhost:4000/products/${productId}`, {
//             method: 'PUT',
//             headers: {
//                 'Content-type': 'application/json',
//                 Authorization: `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({
//                 name: name,
//                 description: description,
//                 img: img,
//                 price: price,
//                 isActive: isActive
//             })
//         })
//             .then(res => res.json())
//             .then(data => {
//                 console.log(data, "from update fetch")
//                 if (data) {
//                     Swal.fire({
//                         title: "Successfully edited",
//                         icon: "success",
//                         text: "You have successfully edited the item."
//                     })


//                 } else {
//                     Swal.fire({
//                         title: "Something went wrong",
//                         icon: "error",
//                         text: "Please try again."
//                     })
//                 }

//                 setName(data.name);
//                 setDescription(data.description);
//                 setImg(data.img);
//                 setPrice(data.price);
//                 setIsActive(data.isActive);
//             })
//     }



//     // Non admin
//     useEffect(() => {
//         fetch(`http://localhost:4000/products/${productId}`)
//             .then(res => res.json())
//             .then(data => {

//                 setName(data.name);
//                 setDescription(data.description);
//                 setPrice(data.price);
//                 setQuantity(data.quantity);
//                 setImg(data.img);
//                 setIsActive(data.isActive)

//                 decQty();
//                 incQty();
//                 // console.log(data, "from productView")
//             })

//     }, [productId])



//     return (
//         (user.isAdmin === false || user.id === null) ?
//             <Container className="mt-5">
//                 <Row>
//                     <Col lg={{ span: 6, offset: 3 }}>
//                         <Card>
//                             <Card.Body className="text-center">
//                                 <img src={img} className="card-img" />
//                                 <Card.Title>{name}</Card.Title>
//                                 <Card.Subtitle>Description:</Card.Subtitle>
//                                 <Card.Text>{description}</Card.Text>
//                                 <Card.Subtitle>Price:</Card.Subtitle>
//                                 <Card.Text>Php {price}</Card.Text>
//                                 <Card.Subtitle>Quantity</Card.Subtitle>
//                                 <Card.Text>
//                                     <Button
//                                         variant="outline-dark"
//                                         className="quantity-btn"
//                                         onClick={() => decQty()}
//                                     >-</Button>
//                                     {quantity}
//                                     <Button
//                                         variant="outline-dark"
//                                         className="quantity-btn"
//                                         onClick={() => incQty()}
//                                     >+</Button>
//                                 </Card.Text>
//                                 {
//                                     (user.id !== null) ?
//                                         <div>
//                                             <Button variant="primary" onClick={() => purchase(productId)}>Purchase</Button>
//                                             <Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>
//                                         </div>
//                                         :
//                                         <Link className="btn btn-danger" to="/login">Login to Purchase</Link>

//                                 }
//                                 {/* <Button variant="primary" onClick={() => purchase(productId)}>Purchase</Button> */}
//                                 {/* <Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button> */}
//                             </Card.Body>
//                         </Card>
//                     </Col>
//                 </Row>
//             </Container>
//             :
//             <div>
//                 <Container>
//                     <h2>Update product info</h2>
//                     <Form className="mt-3" onSubmit={() => update(productId)} >
//                         <Form.Group className="mb-3" controlId="name">
//                             <Form.Label>Product Name</Form.Label>
//                             <Form.Control
//                                 type="text"
//                                 placeholder="Enter product name"
//                                 value={name}
//                                 onChange={e => setName(e.target.value)}
//                                 required />
//                         </Form.Group>

//                         <Form.Group className="mb-3" controlId="description">
//                             <Form.Label>Product Description</Form.Label>
//                             <Form.Control
//                                 type="text"
//                                 placeholder="Enter product description"
//                                 value={description}
//                                 onChange={e => setDescription(e.target.value)}
//                                 required />
//                         </Form.Group>

//                         <Form.Group className="mb-3" controlId="userEmail">
//                             <Form.Label>Image</Form.Label>
//                             <Form.Control
//                                 type="text"
//                                 placeholder="Enter image"
//                                 value={img}
//                                 onChange={e => setImg(e.target.value)}
//                                 required />
//                         </Form.Group>

//                         <Form.Group className="mb-3" controlId="price">
//                             <Form.Label>Enter price</Form.Label>
//                             <Form.Control
//                                 type="text"
//                                 placeholder="Enter product price"
//                                 value={price}
//                                 onChange={e => setPrice(e.target.value)}
//                                 required />
//                         </Form.Group>

//                         <Form.Group className="mb-3" controlId="isActive">
//                             <Form.Label>IsActive?</Form.Label>
//                             <Form.Control
//                                 type="text"
//                                 value={isActive}
//                                 onChange={e => setIsActive(e.target.value)}
//                                 required />
//                         </Form.Group>


//                         <Button variant="primary" type="submit" id="submitBtn">
//                             Update
//                         </Button>
//                     </Form>

//                 </Container>
//             </div >

//     )

// }
// This is testing