// This is working
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState, useContext } from 'react';
import { Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import CartCard from '../components/CartCard';
import UserContext from '../UserContext'; //added 0409


export default function Products() {

    const { user } = useContext(UserContext); //added 0409

    const [products, setProducts] = useState([]);

    const [cartItems, setCartItems] = useState([])
    // Function for handling add to cart
    // const handleClick = (product) => {
    //     // console.log(product)
    //     // cartItems.push(product._id);
    //     // console.log(cartItems)
    //     console.log('Add to Cart')
    //     setCartItems([...cartItems, product])

    // }



    // Fetch products
    // useEffect(() => {

    //     fetch('http://localhost:4000/products/')
    //         .then(res => res.json())
    //         .then(data => {

    //             console.log(data, "from products fetch")
    //             setProducts(data.map(product => {

    //                 return (
    //                     <ProductCard key={product._id} productProp={product} handleClick={handleClick} />
    //                     // <Fragment>
    //                     //     <ProductCard key={product._id} productProp={product} />

    //                     // </Fragment>
    //                 )
    //             }))

    //         })

    // }, [])

    useEffect(() => {
        if (user.isAdmin === false || user.id === null) {
            fetch('https://limitless-waters-59300.herokuapp.com/products/')

                .then(res => res.json())
                .then(data => {

                    // console.log(data, "from products fetch")
                    setProducts(data.map(product => {

                        return (
                            // <ProductCard key={product._id} productProp={product} handleClick={handleClick} />
                            <ProductCard key={product._id} productProp={product} />
                        )
                    }))
                })

        } else if (user.isAdmin === true) {
            fetch('https://limitless-waters-59300.herokuapp.com/products/all')

                .then(res => res.json())
                .then(data => {

                    // console.log(data, "from products fetch")
                    setProducts(data.map(product => {

                        return (
                            <ProductCard key={product._id} productProp={product} />
                        )
                    }))

                })
        }


    }, [])




    return (
        <Container>
            <h1 className="text-center mt-3 mb-3">Products</h1>
            <div className="row justifiy-content-center">
                {products}
            </div>
        </Container>
    )
}

ProductCard.propTypes = {

    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })

}


// This is working

// This is testing
// import PropTypes from 'prop-types';
// import { Fragment, useEffect, useState, useContext } from 'react';
// import { Container } from 'react-bootstrap';
// import ProductCard from '../components/ProductCard';
// import CartCard from '../components/CartCard';
// import UserContext from '../UserContext'; //added 0409
// import Cart from './Cart';


// export default function Products() {

//     // console.log(props)

//     const { user } = useContext(UserContext); //added 0409

//     const [products, setProducts] = useState([]);

//     const [cartItems, setCartItems] = useState([]);
//     // Function for handling add to cart
//     const addToCart = (product) => {
//         // console.log(product)
//         cartItems.push(product._id);
//         // console.log(cartItems)
//         console.log(cartItems)
//         setCartItems([...cartItems, product])

//     }



//     // Fetch products
//     // useEffect(() => {

//     //     fetch('http://localhost:4000/products/')
//     //         .then(res => res.json())
//     //         .then(data => {

//     //             console.log(data, "from products fetch")
//     //             setProducts(data.map(product => {

//     //                 return (
//     //                     <ProductCard key={product._id} productProp={product} handleClick={handleClick} />
//     //                     // <Fragment>
//     //                     //     <ProductCard key={product._id} productProp={product} />

//     //                     // </Fragment>
//     //                 )
//     //             }))

//     //         })

//     // }, [])

//     useEffect(() => {
//         if (user.isAdmin === false || user.id === null) {
//             fetch('http://localhost:4000/products/')

//                 .then(res => res.json())
//                 .then(data => {

//                     // console.log(data, "from products fetch")
//                     setProducts(data.map(product => {

//                         return (
//                             // <ProductCard key={product._id} productProp={product} handleClick={handleClick} />
//                             <ProductCard key={product._id} productProp={product} addToCart={addToCart} cartItems={cartItems} />
//                             // <ProductCard key={product._id} productProp={product} />
//                         )
//                     }))
//                 })

//         } else if (user.isAdmin === true) {
//             fetch('http://localhost:4000/products/all')

//                 .then(res => res.json())
//                 .then(data => {

//                     // console.log(data, "from products fetch")
//                     setProducts(data.map(product => {

//                         return (
//                             <ProductCard key={product._id} productProp={product} />
//                         )
//                     }))

//                 })
//         }


//     }, [])




//     return (
//         <Container>
//             <h1 className="text-center mt-3 mb-3">Products</h1>
//             <div className="row justifiy-content-center">
//                 {products}
//             </div>
//         </Container>
//     )
// }

// ProductCard.propTypes = {

//     productProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired
//     })

// }

// This is testing


