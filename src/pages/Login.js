import { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
// import Logo from '../images/logo.png'
import Header from '../components/Header';

export default function Login() {

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState('');

    function loginUser(e) {

        e.preventDefault();


        fetch('https://limitless-waters-59300.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {

                // console.log(data)

                if (typeof data.access !== 'undefined') {

                    localStorage.setItem('token', data.access)
                    retrieveUserDetails(data.access)

                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to sweets at south dessert house!"
                    })
                } else {

                    Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Check your login details and try again!"

                    })

                }
            })


        setEmail('');
        setPassword('');

    }

    const retrieveUserDetails = (token) => {

        fetch('https://limitless-waters-59300.herokuapp.com/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {


                setUser({
                    id: data._id,
                    firstName: data.firstName, // Addition
                    lastName: data.lastName, // Addition
                    isAdmin: data.isAdmin
                })

            })

    }

    useEffect(() => {
        if (email !== '' && password !== '') {

            setIsActive(true)

        } else {

            setIsActive(false)
        }

    }, [email, password])

    return (
        // (user.id !== null) ?
        //     <Navigate to="/products" />
        //     :
        //     <Container>
        //         <Fragment>
        //             <div>

        //                 <Header />
        //                 <h1>Login</h1>
        //             </div>

        //             <Form onSubmit={(e) => loginUser(e)}>
        //                 <Form.Group className="mb-3" controlId="userEmail">
        //                     <Form.Label>Email address</Form.Label>
        //                     <Form.Control
        //                         type="email"
        //                         placeholder="Enter email"
        //                         value={email}
        //                         onChange={(e) => setEmail(e.target.value)}
        //                         required
        //                     />
        //                     <Form.Text className="text-muted">
        //                         We'll never share your email with anyone else.
        //                     </Form.Text>
        //                 </Form.Group>

        //                 <Form.Group className="mb-3" controlId="password">
        //                     <Form.Label>Password</Form.Label>
        //                     <Form.Control
        //                         type="password"
        //                         placeholder="Password"
        //                         value={password}
        //                         onChange={(e) => setPassword(e.target.value)}
        //                         required
        //                     />
        //                 </Form.Group>

        //                 {
        //                     isActive ?
        //                         <Button variant="success" type="submit">
        //                             Login
        //                         </Button>
        //                         :
        //                         <Button variant="success" type="submit" disabled>
        //                             Login
        //                         </Button>
        //                 }
        //             </Form>

        //         </Fragment >
        //     </Container>




        (user.isAdmin === false) ?
            <Navigate to="/products" />
            :
            (user.isAdmin === true) ?
                <Navigate to="/create" />
                :
                <Container>
                    <Fragment>
                        <div>
                            {/* <Header /> */}
                            {/* <img src="http://drive.google.com/uc?export=view&id=1MlWoNKV0B4tElo9UrHL9c8tMeWntWc-x" className="logo" /> */}
                            <h1>Login</h1>
                        </div>
                        <Form onSubmit={(e) => loginUser(e)}>
                            <Form.Group className="mb-3" controlId="userEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {
                                isActive ?
                                    <Button variant="success" type="submit">
                                        Login
                                    </Button>
                                    :
                                    <Button variant="success" type="submit" disabled>
                                        Login
                                    </Button>
                            }
                        </Form>

                    </Fragment >
                </Container>


    )


}
