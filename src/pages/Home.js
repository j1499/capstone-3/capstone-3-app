import { Fragment } from 'react';
import Hero from '../components/Hero';
import Products from '../pages/Products'
import { Container } from 'react-bootstrap'
// import Header from '../components/Header';

export default function Home() {
    // const { cartItems } = props;
    // console.log(props, "Home")
    return (

        <Fragment>
            <Container>
                <Hero />
                <Products />
            </Container>
        </Fragment>
    )

}