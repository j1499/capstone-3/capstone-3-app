// // This is working
import { Fragment, useEffect, useState, useContext } from 'react';
import { Container } from 'react-bootstrap';
import CartCard from '../components/CartCard';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
// import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';

export default function Cart() {

    const [product, setProducts] = useState([]);




    // Fetch products
    // useEffect(() => {

    //     fetch('http://localhost:4000/users/myCart')
    //         .then(res => res.json())
    //         .then(data => {

    //             console.log(data, "from cart fetch")
    //             setProducts(data.map(product => {

    //                 return (

    //                     <CartCard key={product._id} productProp={product} />

    //                 )
    //             }))

    //         })

    // }, [])

    //  Function for purchase
    // const purchase = (productId) => {

    //     fetch('http://localhost:4000/users/checkout', {
    //         method: 'POST',
    //         headers: {
    //             "Content-Type": "application/json",
    //             Authorization: `Bearer ${localStorage.getItem('token')}`
    //         },
    //         body: JSON.stringify({
    //             productId: productId,
    //             name: name,
    //             description: description,
    //             price: price,
    //             quantity: quantity
    //         })
    //     })
    //         .then(res => res.json())
    //         .then(data => {

    //             // console.log(data, "This is the data from productView")

    //             if (data) {
    //                 Swal.fire({
    //                     title: "Successfully purchased",
    //                     icon: "success",
    //                     text: "You have successfully purchased this product."
    //                 })
    //                 navigate("/products")

    //             } else {
    //                 Swal.fire({
    //                     title: "Something went wrong",
    //                     icon: "error",
    //                     text: "Please try again."
    //                 })
    //             }

    //         })

    // }


    useEffect(() => {

        fetch('https://limitless-waters-59300.herokuapp.com/users/myCart', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                console.log(data, "from cart fetch")
                setProducts(data.map(product => {

                    return (

                        < CartCard key={product._id} cardProp={product} />
                        // console.log(product.totalAmount)


                    )
                }))

            })

    }, [])


    return (
        <Container>
            <Fragment>
                <h1>My Cart</h1>
                {product}
                <h2>Total Price:</h2>

            </Fragment>
        </Container>

    )
}
// This is working

// import { useEffect, Fragment } from 'react';

// const Cart = ({ cart, setCart, handleChange }) => {
// 	const [price, setPrice] = useState(0);

// 	const handleRemove = (id) => {
// 		const arr = cart.filter((product) => product.id !== id);
// 		setCart(arr);
// 		handlePrice();
// 	}

// 	const handlePrice = () => {
// 		let ans = 0;
// 		cart.map((product) => (ans += product.amount * product.price))
// 		setPrice(ans)
// 	}

// 	useEffect(() => {
// 		handlePrice();
// 	})

// 	return (


// 		{
// 			cart.map((product) => (
// 				<div>
// 					<Button onClick={() => handleChange(product, 1)}>+</Button>
// 					<Button onClick={() => handleChange(product, -1)}>-</Button>
// 				</div>
// 			)
// 			)
// 		}


// // import React, { useState, Fragment } from 'react'
// // import ProductCard from '../components/ProductCard';



// // export default function Cart({ cartItems }) {

// // 	// const [cartItems, setCartItems] = useState([]);
// // 	// console.log(cartItems)

// // 	return (
// // 		<Fragment>
// // 			<div>
// // 				<h2>Cart is shown here</h2>
// // 				{/* {cartItems.length === 0 && (<div>No items added.</div>)} */}
// // 			</div>
// // 			{/* <div>
// // 				{cartItems.map((item) => (
// // 					<div key={item.id} />
// // 				))}
// // 			</div> */}
// // 			{cartItems}
// // 		</Fragment>


// // 	)
// // }


// This is testing
// import React from 'react';
// import ProductCard from '../components/ProductCard';
// import CartCard from '../components/CartCard';


// const Cart = ({ props }) => {
//     // const { cartItems } = props;
//     console.log(props)
//     return (
//         <div>
//             {/* <div>Cart is shown here</div>
//             {cartItems.length === 0 && <div>No items added</div>} */}
//             <div>Cart</div>

//         </div>
//     )
// }

// export default Cart
// This is testing




