import React, { Fragment, useState, useEffect } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import Header from '../components/Header'
import Swal from 'sweetalert2';

export default function CreateProduct() {

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [img, setImg] = useState('');
    const [price, setPrice] = useState('')

    const addProduct = (e) => {
        e.preventDefault();

        fetch('https://limitless-waters-59300.herokuapp.com/products/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                img: img,
                price: price
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data) {
                    Swal.fire({
                        title: 'Successfuly created a product',
                        icon: 'success',
                        text: 'View created product at products page'
                    })

                } else {
                    Swal.fire({
                        title: 'error in creating a product',
                        icon: 'error',
                        text: 'error occurred'
                    })
                }
            })

        setName('');
        setDescription('');
        setImg('');
        setPrice('');

    }



    // useEffect(() => {



    // }, [])



    return (

        <div>
            <Container>
                <Header />
                <h2>Create a product</h2>
                <Form className="mt-3" onSubmit={(e) => addProduct(e)}>
                    <Form.Group className="mb-3" controlId="name">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter product name"
                            value={name}
                            onChange={e => setName(e.target.value)}
                            required />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="description">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter product description"
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                            required />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="userEmail">
                        <Form.Label>Image</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter image"
                            value={img}
                            onChange={e => setImg(e.target.value)}
                            required />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="price">
                        <Form.Label>Enter price</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter product price"
                            value={price}
                            onChange={e => setPrice(e.target.value)}
                            required />
                    </Form.Group>
                    <Button variant="primary" type="submit" id="submitBtn">
                        Create
                    </Button>
                </Form>
            </Container>
        </div>

    )

}
