import React, { useEffect, useState } from 'react'
import OrderCard from '../components/OrderCard';
import { Card, Button, Container } from 'react-bootstrap';

export default function Orders() {

    const [orders, setOrders] = useState([]);
    const [productName, setProductName] = useState('');
    const [quantity, setQuantity] = useState('');
    const [totalAmount, setTotalAmount] = useState('');

    useEffect(() => {

        fetch('https://limitless-waters-59300.herokuapp.com/users/myOrders', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                setOrders(data.map(order => {

                    return (

                        <OrderCard key={order._id} orderProp={order} />
                    )
                }))

            })
    }, [])



    return (

        <Container>

            <h1 className="text-center mt-3 mb-3">My orders ({orders.length})</h1>
            <div
                className="row justifiy-content-center">
                {orders}
            </div>
        </Container>

    )
}
