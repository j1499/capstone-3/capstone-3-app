import { useState, useEffect, Fragment } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Cart from '../pages/Cart';
import Swal from 'sweetalert2';


export default function CartCard({ cardProp, addToCart }) {

    // console.log(addToCart)


    console.log(cardProp, "this is the productProp from CartCard.js");
    const { productName, description, price, quantity, totalAmount, productId, _id } = cardProp;
    const navigate = useNavigate();

    const [qty, setQty] = useState(quantity);

    // Function for quantity
    const incQty = () => {
        setQty(quantity + 1)
    }

    const decQty = () => {
        setQty(quantity - 1)
    }

    // Function for quantity
    // const incQty = () => {
    //     return quantity + 1
    // }

    // const decQty = () => {
    //     return quantity - 1
    // }


    // useEffect(() => {
    //     decQty();
    //     incQty();
    // }, [quantity, qty])

    // Function for purchase
    const purchase = (productId) => {

        fetch('https://limitless-waters-59300.herokuapp.com/users/checkout', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                name: productName,
                description: description,
                price: price,
                quantity: quantity,
            })
        })
            .then(res => res.json())
            .then(data => {

                // console.log(data, "This is the data from productView")

                if (data) {
                    Swal.fire({
                        title: "Successfully purchased",
                        icon: "success",
                        text: "You have successfully purchased this product."
                    })
                    navigate("/myorders")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })

    }




    return (
        <Row>
            <Col>
                <Card>
                    <Card.Body>
                        <Card.Title>{productName}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Quantity</Card.Subtitle>
                        <Card.Text>
                            <Button variant="outline-dark"
                                className="quantity-btn"
                                onClick={() => decQty()}
                            >-</Button>
                            {quantity}
                            {/* {setQty} */}
                            <Button variant="outline-dark"
                                className="quantity-btn"
                                onClick={() => incQty()}
                            >+</Button>
                        </Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>Php {price}</Card.Text>
                        <Card.Subtitle>Sub-total:</Card.Subtitle>
                        <Card.Text>Php {totalAmount}</Card.Text>
                        <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
                        <Button className="btn btn-primary" onClick={() => purchase(productId)}>Purchase</Button>
                        <Button className="btn btn-primary">Remove</Button>
                    </Card.Body>
                </Card>
            </Col>

        </Row>

    )
}

// This is testing
// import { useState, useEffect, Fragment } from 'react';
// import { Row, Col, Card, Button } from 'react-bootstrap';
// import { Link, useNavigate } from 'react-router-dom';
// import Cart from '../pages/Cart';
// import Swal from 'sweetalert2';


// export default function CartCard({ cardProp, addToCart }) {

//     // console.log(addToCart)


//     console.log(cardProp, "this is the productProp from CartCard.js");
//     const { productName, description, price, quantity, totalAmount, productId, _id } = cardProp;
//     const navigate = useNavigate();

//     const [qty, setQty] = useState(quantity);

//     // Function for quantity
//     const incQty = () => {
//         setQty(quantity + 1)
//     }

//     const decQty = () => {
//         setQty(quantity - 1)
//     }

//     // Function for quantity
//     // const incQty = () => {
//     //     return quantity + 1
//     // }

//     // const decQty = () => {
//     //     return quantity - 1
//     // }


//     // useEffect(() => {
//     //     decQty();
//     //     incQty();
//     // }, [quantity, qty])

//     // Function for purchase
//     const purchase = (productId) => {

//         fetch('http://localhost:4000/users/checkout', {
//             method: 'POST',
//             headers: {
//                 "Content-Type": "application/json",
//                 Authorization: `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({
//                 productId: productId,
//                 name: productName,
//                 description: description,
//                 price: price,
//                 quantity: quantity,
//             })
//         })
//             .then(res => res.json())
//             .then(data => {

//                 // console.log(data, "This is the data from productView")

//                 if (data) {
//                     Swal.fire({
//                         title: "Successfully purchased",
//                         icon: "success",
//                         text: "You have successfully purchased this product."
//                     })
//                     navigate("/myorders")

//                 } else {
//                     Swal.fire({
//                         title: "Something went wrong",
//                         icon: "error",
//                         text: "Please try again."
//                     })
//                 }

//             })

//     }






//     return (
//         <Row>
//             <Col>
//                 <Card>
//                     <Card.Body>
//                         <Card.Title>{productName}</Card.Title>
//                         <Card.Subtitle>Description:</Card.Subtitle>
//                         <Card.Text>{description}</Card.Text>
//                         <Card.Subtitle>Quantity</Card.Subtitle>
//                         <Card.Text>
//                             <Button variant="outline-dark"
//                                 className="quantity-btn"
//                                 onClick={() => decQty()}
//                             >-</Button>
//                             {quantity}
//                             {/* {setQty} */}
//                             <Button variant="outline-dark"
//                                 className="quantity-btn"
//                                 onClick={() => incQty()}
//                             >+</Button>
//                         </Card.Text>
//                         <Card.Subtitle>Price:</Card.Subtitle>
//                         <Card.Text>Php {price}</Card.Text>
//                         <Card.Subtitle>Sub-total:</Card.Subtitle>
//                         <Card.Text>Php {totalAmount}</Card.Text>
//                         <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
//                         <Button className="btn btn-primary" onClick={() => purchase(productId)}>Purchase</Button>
//                         <Button className="btn btn-primary">Remove</Button>
//                     </Card.Body>
//                 </Card>
//             </Col>

//         </Row>

//     )
// }

