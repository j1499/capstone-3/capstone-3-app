import { Carousel, Button, Row, Col } from 'react-bootstrap';
// import prodImg1 from '../images/img-01.jpg';
import prodImg2 from '../images/hero/img-02.jpg';
import prodImg3 from '../images/hero/img-03.jpg';

export default function Hero() {

    return (
        // <div className="hero-container-1">
        //     <h2>Sweets at south dessert house</h2>
        //     <p>Sweetest house in the south</p>
        //     <Button variant="primary">Learn more</Button>
        // </div>
        <Carousel fade>

            <Carousel.Item>
                <img
                    className="d-block w-100 product-hero-img"
                    src={prodImg2}
                    alt="Second slide"
                />

                {/* <Carousel.Caption className="product-hero-caption">
                    <h3>SansRival Chocolate Cake</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption> */}
            </Carousel.Item>

            <Carousel.Item>
                <img
                    className="d-block w-100 product-hero-img"
                    src={prodImg3}
                    alt="Third slide"
                />

                {/* <Carousel.Caption className="product-hero-caption">
                    <h3>Nama Keto Truffles Chocolate</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </Carousel.Caption> */}
            </Carousel.Item>

        </Carousel>
    )

}