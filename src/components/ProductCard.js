// This is working
import { Fragment, useContext, useEffect, useState } from 'react';
import { Row, Col, Card, Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext'; //added 0409


export default function ProductCard({ productProp, handleClick, product }) {

    const { user } = useContext(UserContext); //added 0409


    console.log(productProp, "this is the productProp from ProduCard.js");
    const { name, description, price, _id, totalAmount, img, isActive } = productProp;



    return (
        // User card
        (user.isAdmin === false || user.id === null) ?
            <Fragment>
                <div className="col-11 col-md-6 col-lg-3 mx-0 mb-4">
                    <div className="card p-0 overflow-hidden h-100 shadow">
                        <Card>
                            <Card.Body>
                                <Card.Title>{name}</Card.Title>
                                <img src={img} className="card-img" />
                                <Card.Title>Description:</Card.Title>
                                <Card.Subtitle>{description}</Card.Subtitle>
                                <Card.Title>Price:</Card.Title>
                                <Card.Subtitle className="pb-3">Php {price}</Card.Subtitle>
                                {/* <Button onClick={() => handleClick(productProp)}>Add to Cart</Button> */}

                                <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
                                {/* <Button onClick={() => addToCart(product)}>Add to Cart</Button> */}

                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </Fragment>
            :
            // Admin card
            <Fragment>
                <div>
                    <div className="card p-0 overflow-hidden h-100 shadow">
                        <Card>
                            <Card.Body>
                                <Card.Title>{name}</Card.Title>
                                <img src={img} className="admin-card-img" />
                                <Card.Title>Description:</Card.Title>
                                <Card.Subtitle>{description}</Card.Subtitle>
                                <Card.Title>Price:</Card.Title>
                                <Card.Subtitle className="pb-3">Php {price}</Card.Subtitle>
                                <Card.Title>Active status: </Card.Title>
                                <Card.Text>{isActive.toString()}</Card.Text>
                                <Link className="btn btn-primary" to={`/products/${_id}`}>Edit</Link>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </Fragment>
        // <Table striped bordered hover>
        //     <thead>
        //         <tr>
        //             <th>Product name</th>
        //             <th>Product description</th>
        //             <th>Product price</th>
        //             <th>Product status</th>
        //         </tr>
        //     </thead>
        //     <tbody>
        //         <tr>
        //             <td colSpan={1}>{name}</td>
        //             <td colSpan={1}>{description}</td>
        //             <td colSpan={1}>{price}</td>
        //             <td colSpan={1}>{isActive.toString()}</td>

        //         </tr>
        //     </tbody>
        // </Table>


    )
}
// This is working

// This is testing
// import { Fragment, useContext, useEffect, useState } from 'react';
// import { Row, Col, Card, Button } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
// import UserContext from '../UserContext';



// export default function ProductCard({ productProp, handleClick, addToCart, cartItems }) {

//     const { user } = useContext(UserContext);

//     console.log(addToCart)

//     // const { cartItems } = cart

//     console.log(cartItems, "Product.js cartItems")

//     console.log(productProp, "this is the productProp from ProduCard.js");
//     const { name, description, price, _id, totalAmount, img, isActive } = productProp;


//     return (
//         // User card
//         (user.isAdmin === false || user.id === null) ?
//             <Fragment>
//                 <div className="col-11 col-md-6 col-lg-3 mx-0 mb-4">
//                     <div className="card p-0 overflow-hidden h-100 shadow">
//                         <Card>
//                             <Card.Body>
//                                 <Card.Title>{name}</Card.Title>
//                                 <img src={img} className="card-img" />
//                                 <Card.Title>Description:</Card.Title>
//                                 <Card.Subtitle>{description}</Card.Subtitle>
//                                 <Card.Title>Price:</Card.Title>
//                                 <Card.Subtitle className="pb-3">Php {price}</Card.Subtitle>

//                                 {/* <Button onClick={() => handleClick(productProp)}>Add to Cart</Button> */}
//                                 {/* <Button onClick={() => addToCart(productProp)}>Add to Cart</Button> */}
//                                 {/* <Button>Add to Cart</Button> */}

//                                 <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
//                                 <Button onClick={() => addToCart(productProp)} addToCart={addToCart}>Add to Cart</Button >

//                             </Card.Body>
//                         </Card>
//                     </div>
//                 </div>
//             </Fragment>
//             :
//             // Admin card
//             <Fragment>
//                 <div>
//                     <div className="card p-0 overflow-hidden h-100 shadow">
//                         <Card>
//                             <Card.Body>
//                                 <Card.Title>{name}</Card.Title>
//                                 <img src={img} className="admin-card-img" />
//                                 <Card.Title>Description:</Card.Title>
//                                 <Card.Subtitle>{description}</Card.Subtitle>
//                                 <Card.Title>Price:</Card.Title>
//                                 <Card.Subtitle className="pb-3">Php {price}</Card.Subtitle>
//                                 <Card.Title>Active status: </Card.Title>
//                                 <Card.Text>{isActive.toString()}</Card.Text>
//                                 <Link className="btn btn-primary" to={`/products/${_id}`}>Edit</Link>
//                             </Card.Body>
//                         </Card>
//                     </div>
//                 </div>
//             </Fragment>


//     )
// }

// This is testing