import { Button, Row, Col } from 'react-bootstrap';
// import prodImg1 from '../images/img-01.jpg';
// import prodImg2 from '../images/hero/img-02.jpg';
// import prodImg3 from '../images/hero/img-03.jpg';
import Logo from '../images/logo.png'

export default function Header() {

    return (
        <Row>
            <Col className="text-center">
                <img src={Logo} className="logo"></img>
            </Col>
        </Row>

    )

}