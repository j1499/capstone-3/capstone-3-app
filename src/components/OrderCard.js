import { Row, Col, Card, Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function OrderCard({ orderProp }) {


    console.log(orderProp, "this is the productProp from OrderCard.js");
    const { productName, price, quantity, totalAmount, description, purchasedOn } = orderProp;

    return (

        <div className="col-11 col-md-6 col-lg-3 mx-0 mb-4">
            <div className="card p-0 overflow-hidden h-100 shadow">
                <Card>
                    <Card.Body>
                        <Card.Title>{productName}</Card.Title>

                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Quantity:</Card.Subtitle>
                        <Card.Text>{quantity}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text className="pb-3">Php {price}</Card.Text>
                        <Card.Subtitle>Total Amount:</Card.Subtitle>
                        <Card.Text className="pb-3">Php {totalAmount}</Card.Text>
                        <Card.Subtitle>Purchased on:</Card.Subtitle>
                        <Card.Text>{purchasedOn}</Card.Text>

                    </Card.Body>
                </Card>
            </div>
        </div>



    )
}