// import { useState, Fragment, useContext } from 'react';
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
// import { Link } from 'react-router-dom';
// import UserContext from '../UserContext';

// export default function AppNavbar() {

//     const { user } = useContext(UserContext);
//     // console.log(user, "from appnavbar");

//     return (
//         <Navbar bg="light" expand="lg">
//             <Navbar.Brand as={Link} to="/">Sweets at south</Navbar.Brand>
//             <Navbar.Toggle aria-controls="basic-navbar-nav" />
//             <Navbar.Collapse id="basic-navbar-nav">
//                 <Nav className="ml-auto">
//                     <Nav.Link as={Link} to="/">Home</Nav.Link>
//                     <Nav.Link as={Link} to="/products">Products</Nav.Link>

//                     {

//                         (user.id !== null) ?
//                             <Fragment>
//                                 <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
//                                 <Nav.Link as={Link} to="/">
//                                     Hi, {user.firstName}
//                                 </Nav.Link>
//                                 <Nav.Link as={Link} to="/myCart">
//                                     View cart
//                                 </Nav.Link>
//                             </Fragment>
//                             :
//                             <Fragment>
//                                 <Nav.Link as={Link} to="/login">Login</Nav.Link>
//                                 <Nav.Link as={Link} to="/register">Register</Nav.Link>
//                             </Fragment>

//                     }

//                 </Nav>
//             </Navbar.Collapse>
//         </Navbar>
//     )
// }

// This is working
// import { useState, Fragment, useContext } from 'react';
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
// import { Link } from 'react-router-dom';
// import UserContext from '../UserContext';

// export default function AppNavbar() {

//     const { user } = useContext(UserContext);
//     console.log(user, "from appnavbar");

//     return (
// <Navbar bg="light" expand="lg">
//     <Navbar.Brand as={Link} to="/">Sweets at south</Navbar.Brand>
//     <Navbar.Toggle aria-controls="basic-navbar-nav" />
//     <Navbar.Collapse id="basic-navbar-nav">
//         <Nav className="ml-auto">
//             <Nav.Link as={Link} to="/">Home</Nav.Link>
//             <Nav.Link as={Link} to="/products">Products</Nav.Link>

//             {

//                 // (user.id !== null) ?
//                 //     <Fragment>
//                 //         <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
//                 //         <Nav.Link as={Link} to="/">
//                 //             Hi, {user.firstName}
//                 //         </Nav.Link>
//                 //         <Nav.Link as={Link} to="/cart">
//                 //             View cart
//                 //         </Nav.Link>
//                 //         <Nav.Link as={Link} to="/myorders">
//                 //             View orders
//                 //         </Nav.Link>
//                 //     </Fragment>
//                 //     :
//                 //     <Fragment>
//                 //         <Nav.Link as={Link} to="/login">Login</Nav.Link>
//                 //         <Nav.Link as={Link} to="/register">Register</Nav.Link>
//                 //     </Fragment>

//                 (user.isAdmin === false) ?
//                     <Fragment>

//                         <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
//                         <Nav.Link as={Link} to="/">
//                             Hi, {user.firstName}
//                         </Nav.Link>
//                         <Nav.Link as={Link} to="/cart">
//                             View cart
//                         </Nav.Link>
//                         <Nav.Link as={Link} to="/myorders">
//                             View orders
//                         </Nav.Link>
//                     </Fragment>
//                     :
//                     (user.isAdmin === true) ?
//                         <Fragment>
//                             <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
//                             <Nav.Link as={Link} to="/">
//                                 Hi, {user.firstName}
//                             </Nav.Link>
//                             <Nav.Link as={Link} to="/create">CreateProduct</Nav.Link>
//                         </Fragment>
//                         :
//                         <Fragment>
//                             <Nav.Link as={Link} to="/login">Login</Nav.Link>
//                             <Nav.Link as={Link} to="/register">Register</Nav.Link>
//                         </Fragment>

//             }
//         </Nav>
//     </Navbar.Collapse>
// </Navbar >
//     )
// }
// This is working

// This is Testing
import { useState, Fragment, useContext } from 'react';
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';
import { Button, Navbar, Nav, NavItem, NavDropdown, MenuItem, Offcanvas } from 'react-bootstrap';

export default function AppNavbar() {

    const { user } = useContext(UserContext);
    console.log(user, "from appnavbar");

    return (

        <Container>
            <Navbar bg="light" expand={false}>
                <Container fluid>
                    {/* <Navbar.Brand href="">Sweets at south</Navbar.Brand> */}
                    <Navbar.Brand as={Link} to="/">Sweets at south</Navbar.Brand>
                    <Navbar.Toggle aria-controls="offcanvasNavbar" />
                    <Navbar.Offcanvas
                        id="offcanvasNavbar"
                        aria-labelledby="offcanvasNavbarLabel"
                        placement="end"
                    >
                        <Offcanvas.Header closeButton>
                            <Offcanvas.Title id="offcanvasNavbarLabel">Welcome to sweets at south</Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                            <Nav className="justify-content-end flex-grow-1 pe-3">
                                <Nav.Link as={Link} to="/">Home</Nav.Link>
                                <Nav.Link as={Link} to="/products">Products</Nav.Link>
                                {
                                    (user.isAdmin === false) ?
                                        <Fragment>
                                            <Nav.Link as={Link} to="/">
                                                Hi, {user.firstName}
                                            </Nav.Link>
                                            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                            <Nav.Link as={Link} to="/cart">
                                                View cart
                                            </Nav.Link>
                                            <Nav.Link as={Link} to="/myorders">
                                                View orders
                                            </Nav.Link>
                                        </Fragment>
                                        :
                                        (user.isAdmin === true) ?
                                            <Fragment>
                                                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                                <Nav.Link as={Link} to="/">
                                                    Hi, {user.firstName}
                                                </Nav.Link>
                                                <Nav.Link as={Link} to="/create">CreateProduct</Nav.Link>
                                            </Fragment>
                                            :
                                            <Fragment>
                                                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                                                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                                            </Fragment>
                                }
                            </Nav>
                        </Offcanvas.Body>
                    </Navbar.Offcanvas>
                </Container>
            </Navbar>
        </Container>




    )
}
// This is testing