// This is working
// import { useState, useEffect } from 'react'
// import { UserProvider } from './UserContext';
// import { Container } from 'react-bootstrap';
// import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// // import { Routes, Route } from 'react-router-dom';

// // import { Fragment } from 'react';
// // Pages
// import AppNavbar from './components/AppNavbar';
// import Home from './pages/Home';
// import Login from './pages/Login';
// import Register from './pages/Register';
// import Logout from './pages/Logout';
// import Products from './pages/Products';
// import ProductView from './pages/ProductView';
// import Cart from './pages/Cart';
// import Orders from './pages/Orders'
// import CreateProduct from './pages/CreateProduct'

// import CartCard from './components/CartCard'


// // import ProductCard from './components/ProductCard';
// // import Calculator from './pages/Calculator'
// import './App.css';

// function App() {

//   const [user, setUser] = useState({
//     id: null,
//     isAdmin: null
//   })

//   // Function for logout
//   const unsetUser = () => {
//     localStorage.clear();
//   }

//   // Testing
//   const [cartItems, setCartItems] = useState([])
//   // // Function for handling add to cart
//   // const handleClick = (product) => {
//   //   // console.log(product)
//   //   // cartItems.push(product._id);
//   //   // console.log(cartItems)
//   //   console.log('Add to Cart')
//   //   setCartItems([...cartItems, product])

//   // }
//   // Testing




//   // Fetch 
//   useEffect(() => {
//     fetch('http://localhost:4000/users/details', {
//       headers: {
//         Authorization: `Bearer ${localStorage.getItem('token')}`
//       }
//     })
//       .then(res => res.json())
//       .then(data => {

//         if (typeof data._id !== 'undefined') {
//           setUser({
//             id: data._id,
//             isAdmin: data.isAdmin
//           })
//         } else {
//           setUser({
//             id: null,
//             isAdmin: null
//           })
//         }
//       })


//   }, [])




//   return (
//     <UserProvider value={{ user, setUser, unsetUser }}>
//       <Router>
//         <AppNavbar />
//         <Routes>
//           <Route
//             exact path="/"
//             element={<Home />} />

//           <Route
//             exact path="/products"
//             element={<Products />}

//           />

//           <Route
//             exact path="/products/:productId"
//             element={<ProductView />}
//           />

//           <Route
//             exact path="/cart"
//             element={<Cart />}
//             cartItems={cartItems} // added
//           />

//           <Route
//             exact path="/login"
//             element={<Login />}
//           />

//           <Route
//             exact path="/logout"
//             element={<Logout />}
//           />

//           <Route
//             exact path="/register"
//             element={<Register />}
//           />

//           <Route exact path="/myorders"
//             element={<Orders />}
//           />

//           <Route exact path="/create"
//             element={<CreateProduct />}
//           />
//           {/* <Route exact path="*" element={<Error />} /> */}
//         </Routes>
//       </Router>
//     </UserProvider>


//   );
// }

// export default App;
// This is working

// This is testing
import { useState, useEffect } from 'react'
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// import { Routes, Route } from 'react-router-dom';

// import { Fragment } from 'react';
// Pages
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Cart from './pages/Cart';
import Orders from './pages/Orders'
import CreateProduct from './pages/CreateProduct'

import CartCard from './components/CartCard'


// import ProductCard from './components/ProductCard';
// import Calculator from './pages/Calculator'
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // Function for logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Testing
  const [cartItems, setCartItems] = useState([])
  // // Function for handling add to cart
  // const handleClick = (product) => {
  //   // console.log(product)
  //   // cartItems.push(product._id);
  //   // console.log(cartItems)
  //   console.log('Add to Cart')
  //   setCartItems([...cartItems, product])

  // }
  // Testing




  // Fetch 
  useEffect(() => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        if (typeof data._id !== 'undefined') {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })


  }, [])




  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Routes>
          <Route
            exact path="/"
            element={<Home />}
          />

          <Route
            exact path="/products"
            element={<Products />}
            setCartItems={setCartItems}
          />

          <Route
            exact path="/products/:productId"
            element={<ProductView />}

          />

          <Route
            exact path="/cart"
            element={<Cart />}

          />

          <Route
            exact path="/login"
            element={<Login />}
          />

          <Route
            exact path="/logout"
            element={<Logout />}
          />

          <Route
            exact path="/register"
            element={<Register />}
          />

          <Route exact path="/myorders"
            element={<Orders />}
          />

          <Route exact path="/create"
            element={<CreateProduct />}
          />
          {/* <Route exact path="*" element={<Error />} /> */}
        </Routes>
      </Router>
    </UserProvider>


  );
}

export default App;
// This is testing
